---
Edition: Editions REPAS
DépotLégal: 2012
Auteurs: Collectif d'élèves et de professeurs
Titre: Une Fabrique de Libertés
SousTitre: Le Lycée autogéré de Paris
Préface: Préface de Patrick Boumard
Collection: Collection Pratiques Utopiques
ISBN : 978-2-919272-05-1
Pages: 450 pages
Prix: 23
Etat: Disponible
Résumé: |
    Depuis 1982, existe à Paris un lycée public autogéré. Unique en son genre, le LAP (Lycée autogéré de Paris) a relevé le défi d'un fonctionnement collectif pris en charge par les professeurs et les élèves. Gestion du lieu, libre fréquentation, assemblées générales régulières, régulation des conflits par la commission justice, mais aussi interdisciplinarité, voyages, pédagogie alternative, ateliers artistiques et recrutement des profs par cooptation, sont quelques-unes des caractéristiques de cet établissement pas comme les autres.

    Le mot qui définit le mieux ce lycée, c'est celui qu'il a décidé d'adopter dans son titre : Lycée autogéré. Comme le dit Patrick Boumard, le préfacier de ce livre : « Voilà  peut-être la vraie spécificité du Lycée autogéré de Paris : montrer qu'il est possible, et depuis plus de 30 ans, de passer de la liberté pédagogique à l'autogestion comme modèle politique en éducation. »

    Dans la première partie de cet ouvrage, le Lycée est présenté à plusieurs voix dans sa forme et ses activités actuelles. Puis, dans une seconde partie, un des pionniers du Lycée, raconte, presque au jour le jour, la naissance et la première année de ce lieu autogéré qui, depuis 30 ans, a vu passer des milliers d'élèves.
Tags:
- Autogestion
- Education populaire
- Alternatives
SiteWeb: https://www.l-a-p.org/
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/une-fabrique-de-libertes-le-lycee-autogere-de-paris?_ga=2.240906243.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272051-une-fabrique-de-libertes-le-lycee-autogere-de-paris-collectif/
Couverture:
Couleurs:
  Fond: '#ffd74d'
  Texte: '#970515'
  PageTitres: '#c9091c'
Adresse: 393 Rue de Vaugirard, 75015 Paris
Latitude: 48.83482
Longitude: 2.291823
Vidéos:
  https://www.brut.media/fr/international/une-journee-dans-le-lycee-autogere-de-paris-adab8d8f-74dd-4e8d-bff2-8e3d8c3d805c: Une journée dans le Lycée Autogéré de Paris - Brut
---

## Les auteurs

Ouvrage collectif, ce livre a été écrit par plus d'une vingtaine de personnes. Des professeurs bien sûr, certains très anciens dans l'histoire, d'autres plus récents, et des élèves - actuels ou anciens - qui témoignent de ce que leur a apporté une scolarité au LAP. C'est la raison pour laquelle on peut aussi bien le lire d'une traite, que dans le désordre, en « sautant » de la commission justice à la création du LAP, des manières d'enseigner à la façon dont les décisions sont prises, en AG, en groupe de base ou en réunion générale de gestion, au gré de vos questions et de vos envies.

## Extrait

> Faire tous ensemble. Tout faire nous-mêmes. Une personne = une voix, profs et élèves confondus. Le lycée appartient à tous. Pas de répression. On se parle. On doit se parler tout le temps, on peut se
parler de tout. On s’engage.
>
> AG, RGG (réunions générales de gestion), commissions, temps officiels, événements impromptus, moments informels bien que rituels, forment notre ronde. Venez donc un mardi midi, tendez l’oreille, peut-être entendrez-vous le retentissant appel « AGÉÉÉÉÉÉ ! » qui distingue notre élève lapien du commun des autres lycéens. Le LAP a son vocabulaire, ses arcanes, dont certains savent tirer parti. Car ici les principes s’agissent, se pratiquent, se dansent, s’éprouvent plus que ne s’énoncent. C’est ainsi, en participant à ces moments qui ponctuent la semaine, qu’élèves et profs font vivre le lycée et entrent dans son histoire. Nos missions sont très variées : de la plus officielle à la plus informelle, de la plus jouissive à la plus triviale, de la plus confortable à la plus pénible... Pas de hiérarchie entre les tâches, elles ont toutes la même noblesse puisqu’elles font « tourner la boutique ». C’est une affaire collective, une affaire toujours à refaire, un imparfait à composer... Mais une affaire qui tourne en somme.
> -- page 125

## Le commentaire des éditeurs

À lire l'histoire et le quotidien du Lycée autogéré de Paris tel qu'il est décrit dans ce livre deux questions aussitôt nous viennent à l'esprit : « Mais comment donc a-t-il été possible que se crée un tel lycée, tellement atypique, au sein de l'éducation nationale ? » On aura la réponse dans le long texte de Bernard Elman qui raconte les origines du LAP.
La seconde question est : « Mais comment un tel lycée peut-il encore exister en France aujourd'hui, tant ses valeurs et ses principes de fonctionnement sont éloignés de ceux qui dominent aujourd'hui dans l'école ? » Le livre ne donne pas de réponse à cette seconde question. Il montre seulement que cela existe, ce qui est une manière de dire que rien n'est impossible à celles et ceux qui veulent vivre leurs utopies - et non seulement les rêver.


